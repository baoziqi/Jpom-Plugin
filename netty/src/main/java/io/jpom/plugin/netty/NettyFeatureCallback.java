package io.jpom.plugin.netty;

import io.jpom.plugin.ClassFeature;
import io.jpom.plugin.FeatureCallback;
import io.jpom.plugin.MethodFeature;
import io.jpom.plugin.ThymeleafUtil;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;

/**
 * 方法回调
 *
 * @author bwcx_jzy
 * @date 2019/8/13
 */
@Configuration
public class NettyFeatureCallback implements FeatureCallback {

    @Override
    public void postHandle(HttpServletRequest request, ClassFeature classFeature, MethodFeature methodFeature, Object... pars) {
        if (classFeature == ClassFeature.SSH) {
            callbackSsh(request, methodFeature);
        }
    }

    private void callbackSsh(HttpServletRequest request, MethodFeature methodFeature) {
        if (methodFeature == MethodFeature.FILE) {
            String pagePluginHtml = ThymeleafUtil.process("ssh/file", null);
            request.setAttribute(ThymeleafUtil.PAGE_VARIABLE, pagePluginHtml);
        }
    }
}
